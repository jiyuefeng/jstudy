package sprout.jstudy.redis.dao;

import sprout.jstudy.redis.domain.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shaojiaxin
 * Date: 14-1-10
 * Time: 下午5:36
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao {

    public boolean add(User user);

    public boolean delete(long id);

    public boolean update(User user);

    public User getById(long id);

}
