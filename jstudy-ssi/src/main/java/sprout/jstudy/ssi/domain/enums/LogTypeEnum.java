package sprout.jstudy.ssi.domain.enums;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-22
 * Time: 下午12:57
 * To change this template use File | Settings | File Templates.
 */
public enum LogTypeEnum {

    ERROR("error", "错误日志");

    private String key;
    private String desc;

    private LogTypeEnum(String key, String desc) {
        this.key = key;
        this.desc = desc;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
