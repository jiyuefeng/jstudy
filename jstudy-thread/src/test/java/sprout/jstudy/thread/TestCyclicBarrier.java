package sprout.jstudy.thread;

import org.junit.Test;

import java.util.concurrent.CyclicBarrier;

/**
 * Created with IntelliJ IDEA.
 * User: shaojiaxin
 * Date: 14-7-17
 * Time: 上午9:21
 * To change this template use File | Settings | File Templates.
 */
public class TestCyclicBarrier {

    @Test
    public void test() throws InterruptedException {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(4, new Runnable() {
            @Override
            public void run() {
                System.out.println("恭喜大家闯关成功，开始颁奖了");
            }
        });
        CyclicBarrierWorker cyclicBarrierWorker1 = new CyclicBarrierWorker("张三", cyclicBarrier);
        CyclicBarrierWorker cyclicBarrierWorker2 = new CyclicBarrierWorker("李四", cyclicBarrier);
        CyclicBarrierWorker cyclicBarrierWorker3 = new CyclicBarrierWorker("王五", cyclicBarrier);
        CyclicBarrierWorker cyclicBarrierWorker4 = new CyclicBarrierWorker("赵六", cyclicBarrier);
        cyclicBarrierWorker1.start();
        cyclicBarrierWorker2.start();
        cyclicBarrierWorker3.start();
        cyclicBarrierWorker4.start();
        Thread.sleep(1000 * 60);
    }

}
